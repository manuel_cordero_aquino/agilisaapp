﻿using System;

namespace AgilisaApp.Core
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Username { get; set; }

        public Usuario()
        {
        }
    }
}

