﻿using System;

namespace AgilisaApp.Core
{
    public class Producto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }

        public Producto()
        {
        }
    }
}

