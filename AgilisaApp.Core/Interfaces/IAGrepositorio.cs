﻿using System;
using System.Threading.Tasks;

namespace AgilisaApp.Core
{
    public interface IAGOnlinerepositorio
    {
        Task<Usuario> AutenticaUsuatio(string usuario, string password);
    }


    public class AGOnlinerepositorio :IAGOnlinerepositorio
    {
        #region IAGOnlinerepositorio implementation
        public Task<Usuario> AutenticaUsuatio(string usuario, string password)
        {
            var usr = new Usuario
                {Nombre = "Jose", Apellido = "Perez", Id=1, Username="jperez@gmail.com" };

            return Task.FromResult<Usuario>(usr);
        }
        #endregion
        
    }
}

