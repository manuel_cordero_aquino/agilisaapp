﻿using System;

namespace AgilisaApp.Core
{

    public interface IAGOnlineClient
    {
        IAGOnlineClient Create();
    }

    public class AGOnlineClient : IAGOnlineClient
    {
        private static IAGOnlineClient _client;

        public AGOnlineClient()
        {
            
        }

        public IAGOnlineClient Create()
        {
            if (_client == null)
            {
                _client = new AGOnlineClient();
            }

            return _client;
        }
    }
}

