﻿
using Android.App;
using Android.Widget;
using Android.OS;
using AgilisaApp.Core;

namespace AgilisaApp.Droid
{
    [Activity(Label = "MainActivity")]    
    public class MainActivity : Activity
    {
        int count = 1;

        private IAGOnlinerepositorio _onlineRepositorio;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _onlineRepositorio = new AGOnlinerepositorio();

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);
            Button newActivity = FindViewById<Button>(Resource.Id.newActivity);
            
            button.Click += async delegate
            {
                var usr = await _onlineRepositorio.AutenticaUsuatio("","");

                button.Text = string.Format("User: {0} has  {1} clicks!", usr.Nombre, count++);
            };

            newActivity.Click += (sender, e) => StartActivity(typeof(HomeActivity));

        }
    }
}


