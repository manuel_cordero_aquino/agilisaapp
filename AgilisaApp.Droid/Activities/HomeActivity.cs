﻿

using Android.App;
using Android.OS;
using Android.Widget;

namespace AgilisaApp.Droid
{
    [Activity(Label = "HomeActivity")]            
    public class HomeActivity : Activity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Home);

            Name = Intent.GetStringExtra(ContextParameter.NAME);
            LastName = Intent.GetStringExtra(ContextParameter.LAST_NAME);
            UserName = Intent.GetStringExtra(ContextParameter.USER_NAME);
            Id = Intent.GetIntExtra(ContextParameter.ID, -1);

            var userInfoTextView = FindViewById<TextView>(Resource.Id.userInfoTextView);

            userInfoTextView.Text = string.Format("Welcome back {0} {1}!!, \n Username: {2}", Name, LastName, UserName);
        }
    }
}

