﻿

using Android.App;
using Android.OS;
using Android.Widget;
using AgilisaApp.Core;
using Android.Content;

namespace AgilisaApp.Droid
{
    [Activity(Label = "Login", MainLauncher = true, Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        private IAGOnlinerepositorio _onlineRepositorio;

        EditText _userEditText;
        EditText _passwordEditText;
        Button _loginButton;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);

            _userEditText = FindViewById<EditText>(Resource.Id.userEditText);
            _passwordEditText = FindViewById<EditText>(Resource.Id.pswEditText);
            _loginButton = FindViewById<Button>(Resource.Id.loginBtn);

            _onlineRepositorio = new AGOnlinerepositorio();

            _loginButton.Click += OnLoginClick;
                
        }

        public async void OnLoginClick(object sender, System.EventArgs e)
        {
            var user = await _onlineRepositorio.AutenticaUsuatio(_userEditText.Text, _passwordEditText.Text);


            if (user != null)
            {
                var intent = new Intent(this, typeof(HomeActivity));
                intent.PutExtra(ContextParameter.USER_NAME, user.Username);
                intent.PutExtra(ContextParameter.ID, user.Id);
                intent.PutExtra(ContextParameter.NAME, user.Nombre);
                intent.PutExtra(ContextParameter.LAST_NAME, user.Apellido);
                StartActivity(intent);
            }
        }
    }
}

